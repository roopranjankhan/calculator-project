import 'dart:math';

import 'package:calculator_project/model.dart';
import 'package:calculator_project/preferences_service.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'km_to_mil.dart';
import 'firebase_history.dart';
import 'storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController firstController = TextEditingController();
  TextEditingController secondController = TextEditingController();
  final _preferencesService = PreferencesService();
  int endResult = 0, firstNumber = 0, secondNumber = 0;
  double powEndResult = 0, powFirstNum = 0, powSecondNum = 0;
  String output = "";
  CollectionReference calculator =
      FirebaseFirestore.instance.collection("calculator");

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    populateFields();
  }

  void populateFields() async {
    final save = await _preferencesService.getData();
    setState(() {
      firstController.text = save.firstNumber;
      secondController.text = save.secondNumber;
    });
  }

  addTwoNumber() {
    setState(() {
      firstNumber = int.parse(firstController.text);
      secondNumber = int.parse(secondController.text);
      endResult = firstNumber + secondNumber;
    });
  }

  subTwoNumber() {
    setState(() {
      firstNumber = int.parse(firstController.text);
      secondNumber = int.parse(secondController.text);
      endResult = firstNumber - secondNumber;
    });
  }

  mulTwoNumber() {
    setState(() {
      firstNumber = int.parse(firstController.text);
      secondNumber = int.parse(secondController.text);
      endResult = firstNumber * secondNumber;
    });
  }

  divTwoNumber() {
    setState(() {
      firstNumber = int.parse(firstController.text);
      secondNumber = int.parse(secondController.text);
      endResult = firstNumber ~/ secondNumber;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Calculator'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              TextField(
                controller: firstController,
                decoration: const InputDecoration(
                  labelText: "Enter First Number : ",
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              TextField(
                controller: secondController,
                decoration: const InputDecoration(
                  labelText: "Enter Second Number : ",
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                "Result : $endResult",
                style:
                    const TextStyle(fontSize: 25, fontWeight: FontWeight.w700),
              ),
              const SizedBox(
                height: 20,
              ),
              TextButton(
                onPressed: save,
                child: const Text("Save"),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.red, // background
                        onPrimary: Colors.white, // foreground
                      ),
                      onPressed: () async {
                        await calculator.add({
                          'firstNumber': firstNumber,
                          'secondNumber': secondNumber,
                          'endResult': endResult
                        }).then((value) => "Data added");
                        addTwoNumber();
                        firstController.clear();
                        secondController.clear();
                      },
                      child: const Text("+")),
                  const SizedBox(
                    width: 20,
                  ),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.red, // background
                        onPrimary: Colors.white, // foreground
                      ),
                      onPressed: () async {
                        await calculator.add({
                          'firstNumber': firstNumber,
                          'secondNumber': secondNumber,
                          'endResult': endResult
                        }).then((value) => "Data added");
                        subTwoNumber();
                        firstController.clear();
                        secondController.clear();
                      },
                      child: const Text("--")),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.red, // background
                        onPrimary: Colors.white, // foreground
                      ),
                      onPressed: () async {
                        await calculator.add({
                          'firstNumber': firstNumber,
                          'secondNumber': secondNumber,
                          'endResult': endResult
                        }).then((value) => "Data added");
                        mulTwoNumber();
                        firstController.clear();
                        secondController.clear();
                      },
                      child: const Text("X")),
                  const SizedBox(
                    width: 20,
                  ),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.red, // background
                        onPrimary: Colors.white, // foreground
                      ),
                      onPressed: () async {
                        await calculator.add({
                          'firstNumber': firstNumber,
                          'secondNumber': secondNumber,
                          'endResult': endResult
                        }).then((value) => "Data added");
                        divTwoNumber();
                        firstController.clear();
                        secondController.clear();
                      },
                      child: const Text("/")),
                  const SizedBox(
                    width: 20,
                  ),
                ],
              ),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.black, // background
                    onPrimary: Colors.white, // foreground
                  ),
                  child: const Text(
                    "->",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => kmtomil()),
                    );
                  },
                )
              ]),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [

                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.black, // background
                    onPrimary: Colors.white, // foreground
                  ),
                  child: const Text(
                    "history",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => history()),
                    );
                  },
                )
              ]
        ),

            ],
          ),
        ),
      ),
    );
  }

  save() {
    final saveData = Save(
        firstNumber: firstController.text, secondNumber: secondController.text);
    _preferencesService.saveData(saveData);
  }
}
