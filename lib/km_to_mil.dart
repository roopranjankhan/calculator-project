import 'package:flutter/material.dart';

import 'calculator.dart';

void main() {
  runApp(const kmtomil());
}

class kmtomil extends StatelessWidget {
  const kmtomil({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  void initState() {
    userInput = 0;
    super.initState();
  }

  final List<String> measures = ['Kilometer', 'Miles'];

  final Map<String, int> measuresMap = {
    'Kilometer': 0,
    'Miles': 1,
  };

  dynamic formulas = {
    '0': [1, 0.621371],
    '1': [1.60934, 1],
  };

  late String resultMessage = '';

  void convert(double value, String from, String to) {
    int? nFrom = measuresMap[from];
    int? nTo = measuresMap[to];
    var multi = formulas[nFrom.toString()][nTo];
    var result = value * multi;

    if (result == 0) {
      resultMessage = 'Cannot Perform this conversion';
    } else {
      resultMessage =
      '${userInput.toString()} $_startMeasure are ${result
          .toString()} $_convertedMeasure';
    }
    setState(() {
      resultMessage = resultMessage;
    });
  }

  late double userInput = 2.34;

  late String _startMeasure = 'Kilometer';
  late String _convertedMeasure = 'Miles';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Km To Miles Converter'),
        ),
        body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(0.0),
          child: Column(children: [
            ElevatedButton(
              child: const Text(
                "<-",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyApp()),
                );
              },
            ),
            TextField(
              onChanged: (text) {
                var input = double.tryParse(text);
                if (input != null) {
                  setState(() {
                    userInput = input;
                  });
                }
              },
              decoration: const InputDecoration(
                labelText: "Enter Input : ",
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              (resultMessage == '') ? '' : resultMessage,
              style: const TextStyle(fontSize: 25, fontWeight: FontWeight.w700),
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.red, // background
                      onPrimary: Colors.white, // foreground
                    ),
                    onPressed: () {
                      if (_startMeasure.isEmpty || _convertedMeasure.isEmpty || userInput == 0) {
                        return;
                      } else {
                        convert(userInput, _startMeasure, _convertedMeasure);
                      }
                    },
                    child: const Text("Convert")),
              ],
            ),
            const Text(
              'From',
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
            ),
            Padding(
                padding:
                const EdgeInsets.symmetric(vertical: 10, horizontal: 40),
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: DropdownButton(
                    style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                        color: Colors.brown),
                    hint: const Text(
                      'Choose a unit',
                      style: TextStyle(color: Colors.brown, fontSize: 20),
                    ),
                    items: measures.map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        _startMeasure = (value) as String;
                      });
                    },
                    value: _startMeasure,
                  ),
                )),
            const Text(
              'To',
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
            ),
            Padding(
                padding:
                const EdgeInsets.symmetric(vertical: 10, horizontal: 40),
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: DropdownButton(
                    style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                        color: Colors.brown),
                    hint: const Text(
                      'Choose a unit',
                      style: TextStyle(color: Colors.brown, fontSize: 20),
                    ),
                    items: measures.map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        _convertedMeasure = (value) as String;
                      });
                    },
                    value: _convertedMeasure,
                  ),
                )
            )

          ]),
        )
        ),
    );
  }
}
