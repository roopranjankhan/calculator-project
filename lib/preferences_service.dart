import 'package:shared_preferences/shared_preferences.dart';

import 'model.dart';


class PreferencesService{
  get firstNumber => null;

  get secondNumber => null;

  Future saveData(Save save) async{
    final preferences = await SharedPreferences.getInstance();

    await preferences.setString('firstNumber', save.firstNumber);
    await preferences.setString('secondNumber', save.secondNumber);
  }

  Future<Save> getData() async{
    final preferences = await SharedPreferences.getInstance();

    await preferences.getString('firstNumber');
    await preferences.getString('secondNumber');

    return Save(firstNumber: firstNumber, secondNumber: secondNumber);
  }
}